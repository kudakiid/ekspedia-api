<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $review = new \App\Models\Review();
        $review->user_id = 1;
        $review->bintang = 3;
        $review->comment = "gaspol";
        $review->save();

        $review2 = new \App\Models\Review();
        $review2->user_id = 2;
        $review2->bintang = 5;
        $review2->comment = "remblong";
        $review2->save();

        $review3 = new \App\Models\Review();
        $review3->user_id = 2;
        $review3->bintang = 5;
        $review3->comment = "koplingloss";
        $review3->save();

        $review4 = new \App\Models\Review();
        $review4->user_id = 2;
        $review4->bintang = 5;
        $review4->comment = "akisoak";
        $review4->save();

        DB::table('reviews')->insert([
            ['user_id' => 2, 'bintang' => 4, 'comment' => 'mantap sekali'],
            ['user_id' => 1, 'bintang' => 2, 'comment' => 'luar biasa'],
            ['user_id' => 1, 'bintang' => 5, 'comment' => 'ggwp']
        ]);
    }
}
