<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GunungFileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gunung_files')->insert([
            ["gunung_id" => 1, "file_id" => 1],
            ["gunung_id" => 1, "file_id" => 2],
            ["gunung_id" => 2, "file_id" => 3],
        ]);
    }
}
