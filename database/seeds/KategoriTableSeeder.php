<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategoris')->insert([
            ['file_id' => 1, 'nama' => 'Tenda'],
            ['file_id' => 2, 'nama' => 'Sepatu'],
            ['file_id' => 3, 'nama' => 'Tas'],
            ['file_id' => 4, 'nama' => 'Survival']
        ]);
    }
}
