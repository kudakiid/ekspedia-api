<?php

use Illuminate\Database\Seeder;

class JalurTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jalur = new \App\Models\Jalur();
        $jalur->gunung_id = 1;
        $jalur->file_id = 3;
        $jalur->nama = "jalur 123";
        $jalur->save();

        $jalur2 = new \App\Models\Jalur();
        $jalur2->gunung_id = 2;
        $jalur2->file_id = 4;
        $jalur2->nama = "jalur 456";
        $jalur2->save();
    }
}
