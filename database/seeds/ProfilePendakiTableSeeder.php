<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfilePendakiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile_porters')->insert([
            ['id' => 1, 'file_id' => 1, 'user_id' => 1, 'nama' => 'Martin', 'alamat' => 'Jalan jalan', 'telepon' => '123123123123', 'usia' => 28, 'gender' => 'male'],
            ['id' => 2, 'file_id' => 2, 'user_id' => 2, 'nama' => 'Joseph', 'alamat' => 'Jalan jalan gang ster', 'telepon' => '45656', 'usia' => 31, 'gender' => 'male']
        ]);
    }
}
