<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            ['file_id'=>1,'nama'=>'BNI','rekening'=>'00822144524334'],
            ['file_id'=>2,'nama'=>'Mandiri','rekening'=>'00822144553424'],
            ['file_id'=>3,'nama'=>'BRI','rekening'=>'55462144524334'],
            ['file_id'=>4,'nama'=>'Britama','rekening'=>'00849824324334'],
        ]);
    }
}
