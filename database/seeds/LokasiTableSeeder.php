<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LokasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lokasis')->insert([
            ['nama' => 'Medan'],
            ['nama' => 'Bandung'],
            ['nama' => 'Jakarta'],
            ['nama' => 'Yogyakarta']
        ]);
    }
}
