<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BarangTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("barangs")->insert([
            ['file_id' => 1, 'kategori_id' => 1, 'brand_id' => 1, 'nama' => 'tenda', 'jumlah' => 1, 'jumlah_unit' => 'buah', 'harga' => 120000, 'harga_per' => 'buah', 'deskripsi' => 'tenda anti air', 'bintang_review' => 3.4],
            ['file_id' => 2, 'kategori_id' => 2, 'brand_id' => 2, 'nama' => 'sepatu', 'jumlah' => 3, 'jumlah_unit' => 'pasang', 'harga' => 50000, 'harga_per' => 'pasang', 'deskripsi' => 'sepatu anti air', 'bintang_review' => 4.7],
            ['file_id' => 3, 'kategori_id' => 2, 'brand_id' => 1, 'nama' => 'tas', 'jumlah' => 2, 'jumlah_unit' => 'buah', 'harga' => 300000, 'harga_per' => 'buah', 'deskripsi' => 'tas anti air', 'bintang_review' => 3.0],
            ['file_id' => 4, 'kategori_id' => 4, 'brand_id' => 2, 'nama' => 'pisau', 'jumlah' => 4, 'jumlah_unit' => 'buah', 'harga' => 35000, 'harga_per' => 'buah', 'deskripsi' => 'pisau anti badai', 'bintang_review' => 4]
        ]);
    }
}
