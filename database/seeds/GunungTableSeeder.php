<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GunungTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gunung = new \App\Models\Gunung();
        $gunung->id = 1;
        $gunung->nama = "Gunung Leuser";
        $gunung->ketinggian = "1000";
        $gunung->latitude = 3.7742;
        $gunung->longitude = 97.2437;
        $gunung->deskripsi = "Gunung yang berada di bagian utara Sumatera";
        $gunung->bintang_review = 3.4;
        $gunung->kesulitan = "medium";
        $gunung->popular_count = 4;
        $gunung->save();

        $gunung2 = new \App\Models\Gunung();
        $gunung2->id = 2;
        $gunung2->nama = "Gunung Rinjani";
        $gunung2->ketinggian = "1500";
        $gunung2->latitude = 8.4113;
        $gunung2->longitude = 116.4573;
        $gunung2->deskripsi = "Gunung yang berada di bagian utara Nusa Tenggara Barat";
        $gunung2->bintang_review = 4.2;
        $gunung2->kesulitan = "hard";
        $gunung->popular_count = 8;
        $gunung2->save();
    }
}
