<?php

use Illuminate\Database\Seeder;

class GunungReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gunungReview = new \App\Models\GunungReview();
        $gunungReview->gunung_id = 1;
        $gunungReview->review_id = 1;
        $gunungReview->save();

        $gunungReview2 = new \App\Models\GunungReview();
        $gunungReview2->gunung_id = 2;
        $gunungReview2->review_id = 2;
        $gunungReview2->save();
    }
}
