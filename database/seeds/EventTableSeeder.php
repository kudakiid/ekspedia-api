<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d H:i:s');

        DB::table('events')->insert([
            ['file_id' => 1, 'nama' => 'BBQ bersama', 'deskripsi' => 'event BBQ an bersama', 'tanggal' => $date],
            ['file_id' => 2, 'nama' => 'Buka puasa bersama', 'deskripsi' => 'event bukber', 'tanggal' => $date],
            ['file_id' => 3, 'nama' => 'Konser Powernap', 'deskripsi' => 'acara konser Powernap', 'tanggal' => $date]
        ]);
    }
}
