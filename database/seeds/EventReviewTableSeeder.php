<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_reviews')->insert([
            ['event_id' => 1, 'review_id' => 5],
            ['event_id' => 2, 'review_id' => 6],
            ['event_id' => 3, 'review_id' => 7],
        ]);
    }
}
