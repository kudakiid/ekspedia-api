<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGunungReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gunung_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gunung_id')->unsigned();
            $table->integer('review_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('gunung_reviews', function (Blueprint $table) {
            $table->foreign('gunung_id')->references('id')->on('gunungs');
            $table->foreign('review_id')->references('id')->on('reviews');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gunung_reviews', function (Blueprint $table) {
            $table->dropForeign('gunung_reviews_gunung_id_foreign');
            $table->dropForeign('gunung_reviews_review_id_foreign');
        });

        Schema::dropIfExists('gunung_reviews');
    }
}

