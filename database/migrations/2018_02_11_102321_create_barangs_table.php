<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_id')->unsigned();
            $table->integer('kategori_id')->unsigned();
            $table->integer('brand_id')->unsigned();
            $table->string('nama');
            $table->integer('jumlah');
            $table->string('jumlah_unit');
            $table->bigInteger('harga');
            $table->string('harga_per');
            $table->text('deskripsi');
            $table->double('bintang_review')->nullable();
        });

        Schema::table('barangs', function (Blueprint $table) {
            $table->foreign('file_id')->references('id')->on('files');
            $table->foreign('kategori_id')->references('id')->on('kategoris');
            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barangs', function (Blueprint $table) {
            $table->dropForeign('barangs_file_id_foreign');
            $table->dropForeign('barangs_kategori_id_foreign');
        });

        Schema::dropIfExists('barangs');
    }
}
