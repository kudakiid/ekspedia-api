<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekomendasiAlatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekomendasi_alats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_id')->unsigned();
            $table->integer('gunung_id')->unsigned();
            $table->string('nama');
            $table->integer('jumlah');
            $table->string('satuan');
            $table->integer('orang');
            $table->text('deskripsi');
            $table->timestamps();
        });

        Schema::table('rekomendasi_alats', function (Blueprint $table) {
            $table->foreign('file_id')->references('id')->on('files');
            $table->foreign('gunung_id')->references('id')->on('gunungs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rekomendasi_alats', function (Blueprint $table) {
            $table->dropForeign('rekomendasi_alats_file_id_foreign');
            $table->dropForeign('rekomendasi_alats_gunung_id_foreign');
        });

        Schema::dropIfExists('rekomendasi_alats');
    }
}
