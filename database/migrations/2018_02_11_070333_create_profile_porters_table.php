<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilePortersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_porters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('nama');
            $table->string('alamat');
            $table->string('telepon');
            $table->integer('usia');
            $table->string('gender');
            $table->timestamps();
        });

        Schema::table('profile_porters' , function (Blueprint $table){
            $table->foreign('file_id')->references('id')->on('files');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_porters', function (Blueprint $table){
            $table->dropForeign('profile_porters_file_id_foreign');
            $table->dropForeign('profile_porters_user_id_foreign');
        });

        Schema::dropIfExists('profile_porters');
    }
}
