<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyaratPeminjamansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('syarat_peminjamans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('toko_id')->unsigned();
            $table->string('persyaratan');
            $table->integer('jumlah');
            $table->text('deskripsi');
            $table->timestamps();
        });

        Schema::table('syarat_peminjamans', function (Blueprint $table) {
            $table->foreign('toko_id')->references('id')->on('tokos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('syarat_peminjamans', function (Blueprint $table) {
            $table->dropForeign('syarat_peminjamans_toko_id_foreign');
        });

        Schema::dropIfExists('syarat_peminjamans');
    }
}
