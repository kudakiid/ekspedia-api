<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangTokosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_tokos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('toko_id')->unsigned();
            $table->integer('barang_id')->unsigned();
            $table->integer('jumlah');
            $table->string('jumlah_unit');
            $table->timestamps();
        });

        Schema::table('barang_tokos', function (Blueprint $table) {
            $table->foreign('toko_id')->references('id')->on('tokos');
            $table->foreign('barang_id')->references('id')->on('barangs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_tokos', function (Blueprint $table) {
            $table->dropForeign('barang_tokos_toko_id_foreign');
            $table->dropForeign('barang_tokos_barang_id_foreign');
        });

        Schema::dropIfExists('barang_tokos');
    }
}
