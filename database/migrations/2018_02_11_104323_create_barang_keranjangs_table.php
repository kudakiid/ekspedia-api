<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangKeranjangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_keranjangs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('keranjang_id')->unsigned();
            $table->integer('barang_id')->unsigned();
            $table->integer('total_barang');
            $table->bigInteger('total_harga');
            $table->timestamps();
        });

        Schema::table('barang_keranjangs', function (Blueprint $table) {
            $table->foreign('keranjang_id')->references('id')->on('keranjangs');
            $table->foreign('barang_id')->references('id')->on('barangs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_keranjangs', function (Blueprint $table) {
            $table->dropForeign('barang_keranjangs_keranjang_id_foreign');
            $table->dropForeign('barang_keranjangs_barang_id_foreign');
        });

        Schema::dropIfExists('barang_keranjangs');
    }
}
