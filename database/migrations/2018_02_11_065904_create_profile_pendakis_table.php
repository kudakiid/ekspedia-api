<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilePendakisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_pendakis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('file_id')->unsigned()->nullable();
            $table->string('nama');
            $table->string('telepon');
            $table->text('alamat');
            $table->timestamps();
        });

        Schema::table('profile_pendakis', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_pendakis', function (Blueprint $table) {
            $table->dropForeign('profile_pendakis_user_id_foreign');
            $table->dropForeign('profile_pendakis_file_id_foreign');
        });

        Schema::dropIfExists('profile_pendakis');
    }
}
