<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTokosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_tokos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('toko_id')->unsigned();
            $table->integer('file_id')->unsigned();
            $table->string('nama');
            $table->timestamps();
        });

        Schema::table('file_tokos', function (Blueprint $table) {
            $table->foreign('toko_id')->references('id')->on('tokos');
            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('file_tokos', function (Blueprint $table) {
            $table->dropForeign('file_tokos_toko_id_foreign');
            $table->dropForeign('file_tokos_file_id_foreign');
        });

        Schema::dropIfExists('file_tokos');
    }
}
