<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/14/2018
 * Time: 2:08 PM
 */

namespace App\Events;


class KeranjangBerubah extends Event
{
    public $keranjangId;
    public $totalBarang;
    public $totalHarga;
    public $perubahan;

    public function __construct($keranjangId, $totalBarang, $totalHarga, $perubahan)
    {
        $this->keranjangId = $keranjangId;
        $this->totalBarang = $totalBarang;
        $this->totalHarga = $totalHarga;
        $this->perubahan = $perubahan;
    }
}