<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 12:23 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    protected $fillable = [
        'id', 'file_id', 'nama', 'deskripsi', 'tanggal'
    ];

    public function eventReview()
    {
        return $this->hasMany('App\Models\EventReview', 'event_id', 'id');
    }

    public function file()
    {
        return $this->belongsTo('App\Models\File', 'file_id', 'id');
    }
}