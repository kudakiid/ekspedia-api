<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/9/2018
 * Time: 11:26 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
    protected $table = 'tokos';

    protected $fillable = [
        'id', 'nama', 'lokasi', 'buka'
    ];

    public function syaratPeminjaman()
    {
        return $this->hasOne('App\Models\SyaratPeminjaman', 'toko_id', 'id');
    }

    public function barangToko()
    {
        return $this->hasMany('App\Models\BarangToko', 'toko_id', 'id');
    }

    public function fileToko()
    {
        return $this->hasMany('App\Models\FileToko', 'toko_id', 'id');
    }

    public function lokasi()
    {
        return $this->belongsTo('App\Models\Lokasi', 'lokasi_id', 'id');
    }

    public function profileKaryawan()
    {
        return $this->hasMany('App\Models\ProfileKaryawan', 'toko_id', 'id');
    }
}