<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 12:09 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'reviews';

    protected $fillable = [
        'id', 'user_id', 'bintang', 'comment'
    ];

    public function barangReview()
    {
        return $this->hasMany('App\Models\BarangReview', 'review_id', 'id');
    }

    public function eventReview()
    {
        return $this->hasOne('App\Models\EventReview', 'review_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function gunungReview()
    {
        return $this->hasOne('App\Models\GunungReview', 'review_id', 'id');
    }
}