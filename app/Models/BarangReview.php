<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 2:12 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BarangReview extends Model
{
    protected $table = 'barang_reviews';

    protected $hidden = ['created_at', 'updated_at', 'merk'];

    protected $fillable = [
        'id', 'barang_id', 'review_id'
    ];

    public function barang()
    {
        return $this->belongsTo('App\Models\Barang', 'barang_id', 'id');
    }

    public function review()
    {
        return $this->belongsTo('App\Models\Review', 'review_id', 'id');
    }
}