<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 2:00 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
        'id', 'nama'
    ];

    public function barang()
    {
        return $this->hasMany('App\Models\Barang', 'brand_id', 'id');
    }
}