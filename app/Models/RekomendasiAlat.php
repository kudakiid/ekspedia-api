<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 1:45 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class RekomendasiAlat extends Model
{
    protected $table = 'rekomendasi_alats';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
        'id', 'file_id', 'gunung_id', 'nama', 'jumlah', 'satuan', 'orang', 'deskripsi'
    ];

    public function gunung()
    {
        return $this->belongsTo('App\Models\Gunung', 'gunung_id', 'id');
    }

    public function file()
    {
        return $this->belongsTo('App\Models\File', 'file_id', 'id');
    }
}