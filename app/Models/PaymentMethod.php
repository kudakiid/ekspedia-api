<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/9/2018
 * Time: 11:56 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $table = 'payment_methods';

    protected $fillable = [
        'id', 'nama', 'deskripsi'
    ];

    public function checkout()
    {
        return $this->hasMany('App\Models\Checkout', 'payment_method_id', 'id');
    }
}