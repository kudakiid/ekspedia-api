<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 3/2/2018
 * Time: 12:09 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected $fillable = [
        'id', 'nama'
    ];

//    public function roleUser()
//    {
//        return $this->hasMany('App\Models\RoleUser', 'role_id', 'id');
//    }

    public function user()
    {
        return $this->belongsToMany('App\Models\User', 'role_users', 'role_id', 'user_id');
    }

    public function menu()
    {
        return $this->hasMany('App\Models\Menu', 'role_id', 'id');
    }
}