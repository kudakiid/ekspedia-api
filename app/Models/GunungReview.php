<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 1:47 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class GunungReview extends Model
{
    protected $table = 'gunung_reviews';

    protected $hidden =['created_at','updated_at'];

    protected $fillable = [
        'id', 'gunung_id', 'review_id'
    ];

    public function review()
    {
        return $this->belongsTo('App\Models\Review', 'review_id', 'id');
    }

    public function gunung()
    {
        return $this->belongsTo('App\Models\Gunung', 'gunung_id', 'id');
    }
}