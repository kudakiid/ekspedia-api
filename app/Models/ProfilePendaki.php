<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 12:41 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProfilePendaki extends Model
{
    protected $table = 'profile_pendakis';

    protected $fillable = [
        'id', 'user_id', 'file_id', 'nama', 'telepon', 'alamat'
    ];

    public function sewaPorter()
    {
        return $this->hasMany('App\Models\ProfilePendaki', 'profile_pendaki_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function file()
    {
        return $this->belongsTo('App\Models\File', 'file_id', 'id');
    }
}