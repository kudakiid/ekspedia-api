<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 2:02 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barangs';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
        'id', 'file_id', 'kategori_id', 'brand_id', 'nama', 'jumlah', 'jumlah_unit', 'harga', 'harga_per', 'deskripsi', 'bintang_review'
    ];

    public function brand()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id', 'id');
    }

    public function barangKeranjang()
    {
        return $this->hasMany('App\Models\BarangKeranjang', 'barang_id', 'id');
    }

    public function barangReview()
    {
        return $this->hasMany('App\Models\BarangReview', 'barang_id', 'id');
    }

    public function barangToko()
    {
        return $this->hasOne('App\Models\BarangToko', 'barang_id', 'id');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Models\Kategori', 'kategori_id', 'id');
    }

    public function file()
    {
        return $this->belongsTo('App\Models\File', 'file_id', 'id');
    }
}