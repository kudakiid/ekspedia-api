<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 12:28 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    protected $table = 'tracks';

    protected $fillable = [
        'id', 'file_id', 'nama'
    ];

    public function file()
    {
        return $this->belongsTo('App\Models\File', 'file_id', 'id');
    }

    public function gunungTrack()
    {
        return $this->hasMany('App\Models\Track', 'gunung_tracks', 'track_id', 'gunung_id');
    }
}