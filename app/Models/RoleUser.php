<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 3/2/2018
 * Time: 12:10 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table = 'role_users';

    protected $fillable = [
        'id', 'user_id', 'role_id'
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}