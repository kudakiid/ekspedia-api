<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/14/2018
 * Time: 12:13 PM
 */

namespace App\Http\Controllers;


use App\Models\Bank;

class BankController extends Controller
{
    public function index()
    {
        $bank = Bank::with('file')->get();

        if ($bank) {
            return $this->jsonResponse([
                "bank" => $bank
            ], false, "berhasil mengambil semua bank");
        }

        return $this->jsonResponse(null, true, "tidak ada bank", 500);
    }
}