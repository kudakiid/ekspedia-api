<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 10:21 PM
 */

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function semuaEvent()
    {
        $event = Event::with('file')->get();

        if ($event) {
            return $this->jsonResponse([
                'event' => $event
            ], false, "berhasil mendapatkan semua event");
        }

        return $this->jsonResponse(null, true, "tidak ada event", 500);
    }

    public function event(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);


    }
}