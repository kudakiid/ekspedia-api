<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 3/2/2018
 * Time: 11:49 AM
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ProfilePendaki;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function registerPendaki(Request $request)
    {
        $this->validate(
            $request,
            [
                'email' => 'required|email|max:255|unique:users,email',
                'username' => 'required|max:255|unique:users,username',
                'password' => 'required|min:6',
                'nama' => 'required|max:255',
                'telepon' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'min:6' => 'diperlukan minimal 6 huruf :attribute',
                'unique' => 'pendaki dengan :attribute tersebut sudah terdaftar'
            ]);

        $newUser = new User();
        $newUser->username = $request->json("username");
        $newUser->password = Hash::make($request->json("password"));
        $newUser->email = $request->json("email");
        $newUser->save();
        $newUser->role()->attach(3);

        $stringToken = (string)$this->generateUserToken($newUser);
        $newUser->token = $stringToken;
        $saveUser = $newUser->save();

        $profilePendaki = new ProfilePendaki();
        $profilePendaki->nama = $request->json("nama");
        $profilePendaki->telepon = $request->json("telepon");
        $profilePendaki->user()->associate($newUser);
        $saveProfilePendaki = $profilePendaki->save();

        if ($saveUser && $saveProfilePendaki) {
            return $this->jsonResponse(
                [
                    'token' => (string)$stringToken
                ], false, "registrasi pendaki berhasil");
        }

        return response()->json([
            $this->jsonResponse(null, true, "gagal menyimpan user dan profile pendaki baru", 500)
        ]);
    }
}