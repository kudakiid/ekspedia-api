<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 6:54 AM
 */

namespace App\Http\Controllers;


use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        $token = $this->parseRequestHeaderStringTokenToToken($request);

        $user = User::find($token->getClaim('id'));

        $profile = $user->profilePendaki()->first();
        $photoProfile = $profile->file()->first();

        if (!$user) {
            return $this->jsonResponse(null, true, "gagal mengambil user, user tidak ditemukan", 422);
        }

        if (!$profile) {
            return $this->jsonResponse(null, true, "gagal mengambil profile, profile tidak ditemukan", 422);
        }

        if (!$photoProfile) {
            return $this->jsonResponse(
                [
                    "username" => $user->username,
                    "email" => $user->email,
                    "nama" => $profile->nama,
                    "telepon" => $profile->telepon,
                    "alamat" => $profile->alamat,
                    "file" => null
                ],
                false,
                "berhasil mengambil profile pendaki tanpa foto profile"
            );
        }

        return $this->jsonResponse(
            [
                "username" => $user->username,
                "email" => $user->email,
                "nama" => $profile->nama,
                "telepon" => $profile->telepon,
                "alamat" => $profile->alamat,
                "file" => [
                    "file_id" => $photoProfile->id,
                    "file_nama" => $photoProfile->nama,
                    "file_path" => $photoProfile->path,
                    "file_type" => $photoProfile->type,
                    "file_size" => $photoProfile->size
                ]
            ],
            false,
            "berhasil mengambil profile pendaki beserta foto profile"
        );
    }

    public function editProfile(Request $request)
    {
        $profilePendaki = $this->savingProfile($request);
        $saveProfilePendaki = $profilePendaki->save();

        if ($saveProfilePendaki) {
            return $this->jsonResponse(null, false, "berhasil menyimpan profile baru");
        }

        return $this->jsonResponse(null, true, "gagal menyimpan profile baru", 500);
    }

    public function editProfileAndPhoto(Request $request)
    {
        $this->validate(
            $request,
            [
                'file' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong'
            ]
        );
        if (!$request->file('file')->isValid()) {
            $this->jsonResponse(null, true, "file tidak valid");
        }

        $clientOriginalExtension = $request->file('file')->getClientOriginalExtension();
        $hashNameWithExtension = $request->file('file')->hashName();
        $hashName = strstr($hashNameWithExtension, '.', true);
        $fileName = $hashName . '.' . $clientOriginalExtension;

        $request->file('file')->move($this->publicStoragePath, $fileName);

        $file = new File();
        $file->nama = $fileName;
        $file->path = $this->publicStoragePath . $fileName;
        $file->type = $request->file('file')->getClientMimeType();
        $file->size = $request->file('file')->getClientSize();
        $saveFile = $file->save();

        if (!$saveFile) {
            return $this->jsonResponse(null, true, "gagal menyimpan file", 500);
        }

        $profilePendaki = $this->savingProfile($request);
        $profilePendaki->file()->associate($file);
        $saveProfilePendaki = $profilePendaki->save();

        if ($saveProfilePendaki) {
            return $this->jsonResponse(null, false, "berhasil menyimpan profile baru dan foto profile");
        }

        return $this->jsonResponse(null, true, "gagal menyimpan profile baru", 500);
    }

    private function savingProfile(Request $request)
    {
        $token = $this->parseRequestHeaderStringTokenToToken($request);
        $profilePendaki = User::find($token->getClaim('id'))->profilePendaki()->first();
        if ($request->has("nama") && $request->json("nama") != "") {
            $profilePendaki->nama = $request->json("nama");
        }
        if ($request->has("telepon") && $request->json("telepon") != "") {
            $profilePendaki->telepon = $request->json("telepon");
        }
        if ($request->has("alamat") && $request->json("alamat") != "") {
            $profilePendaki->alamat = $request->json("alamat");
        }

        return $profilePendaki;
    }
}