<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/14/2018
 * Time: 12:17 PM
 */

namespace App\Http\Middleware;


use App\Models\Keranjang;
use Closure;

class CheckKeranjang extends BaseMiddleware
{
    public function handle($request, Closure $next)
    {
        $this->token = $this->parseRequestHeaderStringTokenToToken($request);

        $keranjang = Keranjang::where([
            ['user_id', $this->token->getClaim('id')],
            ['open', true]
        ])->first();

        if ($keranjang) {
            $request->attributes->add(['keranjang' => $keranjang]);
            return $next($request);
        }

        $newKeranjang = new Keranjang();
        $newKeranjang->user_id = $this->token->getClaim('id');
        $newKeranjang->total_barang = 0;
        $newKeranjang->total_harga = 0;
        $newKeranjang->open = true;
        $newKeranjang->save();

        $request->attributes->add(['keranjang' => $newKeranjang]);
        return $next($request);
    }
}